# virtual lab

*This is a template demonstrating a basic experiment in BEE.*

**AIM: To identify various fundamental electronic components and state their uses.**

**Learning objectives:**
To get acquainted with electronics and its role in daily life.

**Apparatus:**

1.  Resistors
*  carbon composition
*  Fusible resistors
*  Metal oxide resistors
 
2. Capacitors
*  Ceramic capacitors
*  Aluminium and tatantalum capacitors
*  Electrolytic capacitors

3.Inductors
*  Air core
*  Iron core
*  Ferrite core
*  

**Theory:**
>  A resistor is a passive two terminal electrical component that implements electrical resistance as a circuit element.

In electronic circuits resistors are used to reduce current flow, adjust signal levels, to divide voltages, bias active elements etc.

>  A capacitor is a passive 2 terminal electronic component that stores electrical energy in an electric field.

Capacitors are of various types depending on their dielectric materials.

>  An inductor is a passive two terminal electrical component that stores energy in a magnetic field when current flows through it.

An inductor typically consists of an insulated wire wound into a coil around a core.

**Procedure:**
Identify the components according to the features given above.

**Conclusion:**
Got acquainted with basic electronic components such as resistors,inductors and capacitors and their sub types.


